package com.foxgeardevs.dokkanguide.ui.events

import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Chronometer.OnChronometerTickListener
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.foxgeardevs.dokkanguide.R
import kotlinx.android.synthetic.main.fragment_events.*
import java.util.concurrent.TimeUnit


class EventsFragment : Fragment() {
    private var pauseOffset: Long = 0
    private var running = false
    var timesLaps: ArrayList<String> = ArrayList()
    var adapter: MyRecyclerViewAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_events, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()
        chronometer.format = "Time: %s"
        chronometer.base = SystemClock.elapsedRealtime()
        chronometer.onChronometerTickListener =
            OnChronometerTickListener { chronometer ->
                if (SystemClock.elapsedRealtime() - chronometer.base >= 10000) {
                    chronometer.base = SystemClock.elapsedRealtime()
                }
            }

        refreshElements()
        btStart.setOnClickListener {
            if (running)
            {
                pauseChronometer()
                btStart.text ="START"
                btRest.text="CLEAR"
                btStart.setBackgroundColor(resources.getColor(R.color.colorActive))
                btRest.setBackgroundColor(resources.getColor(R.color.lightYellow))
            }else
            {
                startChronometer()
                btStart.text ="PAUSE"
                btRest.text="LAP"
                btStart.setBackgroundColor(resources.getColor(R.color.red))
                btRest.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            }
        }
        btRest.setOnClickListener {
            if (running)
            {
                lapChronometer()
            }else
            {
                resetChronometer()
            }
        }
    }

    private fun lapChronometer() {
        if (running) {
            timesLaps.add(transformTime(SystemClock.elapsedRealtime() - chronometer.base))
        }
    }

    private fun startChronometer() {
        if (!running) {
            chronometer.base = SystemClock.elapsedRealtime() - pauseOffset
            chronometer.start()
            running = true
        }
    }

    private fun pauseChronometer() {
        if (running) {
            chronometer.stop()
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.base
            running = false
        }
    }

    private fun resetChronometer() {
        chronometer.base = SystemClock.elapsedRealtime()
        pauseOffset = 0
        timesLaps.clear()
        println("clear")
    }
    private  fun refreshElements()
    {
        rvTimes.layoutManager = LinearLayoutManager(this.context)
        adapter = MyRecyclerViewAdapter(this.context, timesLaps)
        rvTimes.adapter = adapter
    }

    private fun transformTime(millis:Long ) :String {
        return String.format(
            "%02d:%02d:%02d",
            TimeUnit.MILLISECONDS.toHours(millis),
            TimeUnit.MILLISECONDS.toMinutes(millis) -
                    TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
            TimeUnit.MILLISECONDS.toSeconds(millis) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        )
    }
}
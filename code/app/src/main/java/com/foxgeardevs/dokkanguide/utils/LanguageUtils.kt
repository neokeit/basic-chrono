package com.foxgeardevs.dokkanguide.utils

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.util.DisplayMetrics
import com.foxgeardevs.dokkanguide.utils.enums.LanguagesCode
import java.util.*

class LanguageUtils(var context: Context) {
    fun setLanguage(localeCode: String) {
        if (localeCode.isEmpty())
            return
        val resources: Resources = context.resources
        val dm: DisplayMetrics = resources.displayMetrics
        val config: Configuration = resources.configuration
        config.setLocale(Locale(localeCode.toLowerCase(Locale.ROOT)))
        resources.updateConfiguration(config, dm)
    }

    fun getLanguage(): String {
        val sharedPref: SharedPreferences = context.getSharedPreferences("chronoConfig", Context.MODE_PRIVATE)
        val defaultLang = if(context.resources.configuration.locale.language== LanguagesCode.Spanish){LanguagesCode.Spanish}else{LanguagesCode.English}
        return sharedPref.getString("language",defaultLang)?: ""
    }
}

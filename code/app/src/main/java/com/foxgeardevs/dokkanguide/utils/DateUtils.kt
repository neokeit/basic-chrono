package com.foxgeardevs.dokkanguide.utils

import android.annotation.SuppressLint
import android.content.Context
import com.foxgeardevs.dokkanguide.R
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class DateUtils {
    fun formatMillisecond(millisUntilFinished: Long): String {
        var millisSeconds: Long = millisUntilFinished
        val days = TimeUnit.MILLISECONDS.toDays(millisSeconds)
        millisSeconds -= TimeUnit.DAYS.toMillis(days)
        val hours = TimeUnit.MILLISECONDS.toHours(millisSeconds)
        millisSeconds -= TimeUnit.HOURS.toMillis(hours)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(millisSeconds)
        millisSeconds -= TimeUnit.MINUTES.toMillis(minutes)
        val seconds = TimeUnit.MILLISECONDS.toSeconds(millisSeconds)
        return "$days d " + String.format("%02d:%02d:%02d", hours, minutes, seconds)
    }

    @SuppressLint("SimpleDateFormat")
    fun dateDiff(dateEnd: String?): Long {
        if (dateEnd.isNullOrEmpty())
        {
            return 0
        }
        val endDate: Date? = SimpleDateFormat("yyyyMMddHHmm").parse(dateEnd) ?: return 0
        return endDate!!.time - Calendar.getInstance().time.time
    }

    @SuppressLint("SimpleDateFormat")
    fun formatDate(date: String?, context: Context): String {
        if (date.isNullOrEmpty() || date=="00010101")
            return "-"

        val dateValue: Date? = SimpleDateFormat(context.getString(R.string.dateReleasePattern)).parse(date)
        val dateFormat = android.text.format.DateFormat.getLongDateFormat(context)

        if (dateValue == null)
            return "-"

        return dateFormat.format(dateValue)
    }
}